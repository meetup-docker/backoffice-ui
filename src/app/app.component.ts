import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';


import * as $ from 'jquery';

@Component({
  selector: 'my-app',
  moduleId: module.id,
  templateUrl: 'app.component.html'
})

export class AppComponent implements OnInit{
  location: Location;
  constructor(location:Location) {
    this.location = location;
  }
  ngOnInit(){
    $.getScript('../assets/js/material-dashboard.js');
    $.getScript('../assets/js/initMenu.js');
  }
}
