import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { User } from '../models/user';

@Injectable()
export class UserService {
  private baseURL = 'http://0.0.0.0:3000';
  constructor(private http: Http) { }

  getAll() {
    return this.http.get(this.baseURL + '/api/users', this.jwt()).map((response: Response) => response.json());
  }

  getById(id: string) {
    return this.http.get(this.baseURL + '/api/users/' + id, this.jwt()).map((response: Response) => response.json());
  }

  create(user: User) {
    return this.http.post(this.baseURL + '/api/users/register', user, this.jwt()).map((response: Response) => response.json());
  }

  update(user: User) {
    return this.http.put(this.baseURL + '/api/users/' + user.id, user, this.jwt()).map((response: Response) => response.json());
  }

  delete(id: string) {
    return this.http.delete(this.baseURL + '/api/users/' + id, this.jwt()).map((response: Response) => response.json());
  }

  // private helper methods

  private jwt() {
    // create authorization header with jwt token
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser && currentUser.token) {
      let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
      return new RequestOptions({ headers: headers });
    }
  }
}
