import { Component, OnInit, OnDestroy } from '@angular/core';
import {fadeInAnimation} from "../../animations/fade-in.animation";
import {UserService, PubSubService } from '../../services/index';
import { Subscription } from 'rxjs/Subscription';
import {User} from "../../models/user";

@Component({
  selector: 'app-users',
  templateUrl: 'users.component.html',
  styleUrls: ['users.component.css'],
  animations: [fadeInAnimation],
  host: {'[@fadeInAnimation]' : ''}
})
export class UsersComponent implements OnInit, OnDestroy {
  users: User[];
  subscription: Subscription;

  constructor(
    private userService: UserService,
    private pubSubService: PubSubService) { }

  deleteUser(id: string) {
    this.userService.delete(id).subscribe(() => {this.loadAllUsers();});
  }

  ngOnInit() {
    this.loadAllUsers();

    // reload products when updated
    this.subscription = this.pubSubService.on('users-updated').subscribe(() => this.loadAllUsers());
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }

  private loadAllUsers() {
    this.userService.getAll().subscribe((users) => { this.users = users; });
  }
}
