/**
 * Created by rperez on 07/08/17.
 */
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { slideInOutAnimation } from '../../animations/index';
import { UserService, PubSubService} from '../../services/index';

@Component({
  moduleId: module.id.toString(),
  templateUrl: 'users-add-edit.component.html',
  animations: [slideInOutAnimation],
  host: { '[@slideInOutAnimation]': '' }
})

export class UsersAddEditComponent implements OnInit {
  title = "Add User";
  user:any = {};

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private pubSubService: PubSubService) { }

  ngOnInit() {
    let userId = this.route.snapshot.params['id'];
    if (userId) {
      this.title = 'Edit User';
      this.userService.getById(userId).subscribe((user) => { this.user = user });
    }
  }

  saveUser() {
    // save product
    this.userService.create(this.user);

    // redirect to users view
    this.router.navigate(['users']);

    // publish event so list controller can refresh
    this.pubSubService.publish('users-updated');
  }
}
